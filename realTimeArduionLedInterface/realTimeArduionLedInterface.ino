
//#include <SoftwareSerial.h>

//SoftwareSerial mySerial(0, 1); // RX, TX
#include <Servo.h>

Servo servo1;
Servo servo2;

String serialResponse = "";
char * pch;
char * arr[1];
int i = 0;
int xCoor;
int yCoor;
int obje1y;
int obje2y;
int projectStatus = 1;
int objeCoorX;
int objeCoorY;
int delayTime;
int locationCounter;
uint8_t location[] = {A0,A1,A2,A3};
int stepCount;
int activePin;
void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  
  pinMode(A0,OUTPUT);
  pinMode(A1,OUTPUT);
  pinMode(A2,OUTPUT);
  pinMode(A3,OUTPUT);

  
  Serial.begin(115200);
  //mySerial.begin(4800);
  servo1.attach(10);
  servo2.attach(11);
  delayTime = 2;
  stepCount = 4;
}

void loop() {
  
  if ( Serial.available() > 0) {
    restart:
    serialResponse = Serial.readString();
    if(serialResponse == "Proje1"){
      projectStatus = 1;
      digitalWrite(LED_BUILTIN, HIGH);
      goto restart;
    }else if(serialResponse == "Proje2"){
      projectStatus = 2;
      goto restart;
    }else if(serialResponse == "Proje3"){
      projectStatus = 3;
      digitalWrite(LED_BUILTIN, LOW);
      goto restart;
    }
    if(projectStatus == 1){
      int str_len = serialResponse.length() + 1; 
      char char_array[str_len];
      serialResponse.toCharArray(char_array, str_len);
      activePin = atoi(char_array);
      //Serial.println(b);
      if(activePin == 10){
          digitalWrite(12,HIGH);
        }else{
          digitalWrite(activePin,HIGH);
        }
      for (int j = 2; j < 11; j++)
      {
        if   (j!= activePin )
        {
          if(j==10){
            digitalWrite(12,LOW);
          }else{
            digitalWrite(j,LOW);
          }
        
        }
      }
    }else if(projectStatus == 3){
        int str_len = serialResponse.length() + 1; 
        char char_array[str_len];
        serialResponse.toCharArray(char_array, str_len);
        pch = strtok(char_array,"*");
        i = 0;
        while (pch != NULL)
        {
          arr[i++] = pch;
          pch = strtok (NULL, "*");
        }
        obje1y = atoi(arr[0]);
        obje2y = atoi(arr[1]);
  
        obje1y = map(obje1y,0,480,40,180);
        obje2y = map(obje2y,0,480,40,180);
        delay(15);
        servo1.write(obje1y);
        servo2.write(obje2y);
    }
      
    if(projectStatus == 1){
      Serial.print("Project1Recived");
    }else if(projectStatus == 2){
      Serial.print("Project2Recived");
    }else if(projectStatus == 3){
      Serial.print("Project3Recived");
    }
     
  }
  project2();
}
void project2(){
    if(projectStatus == 2){
      if(serialResponse == "left"){
      digitalWrite(LED_BUILTIN, HIGH);
        locationCounter=-1;
        for(int i = 0; i<stepCount; i++){
          locationCounter++;
          digitalWrite(location[locationCounter],HIGH);
          delay(delayTime);
          digitalWrite(location[locationCounter],LOW);
          if(locationCounter == 3) locationCounter=-1;
        }
      }else if(serialResponse == "right"){
        digitalWrite(LED_BUILTIN, LOW);
        locationCounter=4;
        for(int i = 0; i<stepCount; i++){
          locationCounter--;
          digitalWrite(location[locationCounter],HIGH);
          delay(delayTime);
          digitalWrite(location[locationCounter],LOW);
          if(locationCounter == -1) locationCounter=4;
        }
      }
      
     } 
  }
